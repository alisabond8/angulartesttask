import { Injectable } from '@angular/core';
import {InMemoryDbService} from "angular-in-memory-web-api";
import { OwnerEntity } from '../../owners/interfaces/OwnerEntity';

@Injectable({
  providedIn: 'root'
})
export class LocalInMemoryDbService implements InMemoryDbService  {

  constructor() { }

  createDb() {
    let owners: OwnerEntity[] = [
      {
        id: 1,
        firstName: 'John',
        middleName: 'Smith',
        lastName: 'Doe',
        cars: [
          {
            registrationNumber: 'AA1111BB',
            vendor: 'Honda',
            model: 'Civic',
            manufacturedIn: 2020
          }
        ]
      }
    ];
    return {
      owners
    }
  }
}
