import { TestBed } from '@angular/core/testing';

import { LocalInMemoryDbService } from './local-in-memory-db.service';

describe('LocalInMemoryDbService', () => {
  let service: LocalInMemoryDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalInMemoryDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
