import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {CarsResourceService} from "../resources/cars-resource.service";
import {OwnerEntity} from "../interfaces/OwnerEntity";

@Injectable({
  providedIn: 'root'
})
export class OwnerResolver implements Resolve<OwnerEntity | boolean> {
  constructor(
    private carsResource: CarsResourceService
  ) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OwnerEntity | boolean> {
    const action = route.params.action || 'add';
    const id = +route.params.id || 0;
    if (action !== 'add') {
      return this.carsResource.getOwnerById(id);
    }
    return of(true);
  }
}
