import { CarEntity } from "./CarEntity";

export interface OwnerEntity {
  id?: number | undefined;
  firstName: string;
  middleName: string;
  lastName: string;
  cars: CarEntity[]
}
