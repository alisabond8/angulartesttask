export interface CarEntity {
  registrationNumber: string;
  vendor: string;
  model: string;
  manufacturedIn: number;
}
