import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnersListComponent } from './components/owners-list/owners-list.component';
import { OwnerEditComponent } from './components/owner-edit/owner-edit.component';
import {NgxBootstrapIconsModule} from "ngx-bootstrap-icons";
import {icons} from "./icons";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    OwnersListComponent,
    OwnerEditComponent
  ],
  imports: [
    CommonModule,
    NgxBootstrapIconsModule.pick(icons),
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    OwnersListComponent
  ]
})
export class OwnersModule { }
