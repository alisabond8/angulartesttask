import {plusCircle, eye, pencilSquare, arrowLeftCircle, download, trash,} from "ngx-bootstrap-icons";

export const icons = {
  plusCircle,
  eye,
  pencilSquare,
  trash,
  arrowLeftCircle,
  download

}
