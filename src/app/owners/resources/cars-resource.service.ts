import { Injectable } from '@angular/core';
import {ICarOwnersService} from "../interfaces/ICarOwnersService";
import {Observable} from "rxjs";
import {OwnerEntity} from "../interfaces/OwnerEntity";
import {CarEntity} from "../interfaces/CarEntity";
import {HttpService} from "../../core/services/http.service";
import {switchMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CarsResourceService implements ICarOwnersService {

  constructor(
    private http: HttpService
  ) { }

  getOwners(): Observable<OwnerEntity[]> {
    return this.http.get('owners');
  };

  getOwnerById(aId: number): Observable<OwnerEntity> {
    return this.http.get(`owners/${aId}`);
  };

  createOwner(
    aLastName: string,
    aFirstName: string,
    aMiddleName: string,
    aCars: CarEntity[]
  ): Observable<OwnerEntity> {
    return this.getOwners().pipe(
      switchMap((users) => {
        const entity: OwnerEntity = {
          firstName: aFirstName,
          middleName: aMiddleName,
          lastName: aLastName,
          cars: aCars
        };
        // SETTING THIS BECAUSE OF LocalInMemoryDbService BUG
        // IT CAN NOT AUTOMATICALLY GENERATE ID FOR ENTITY IF DB IS EMPTY
        if(!users.length) {
          entity.id = 1;
        }
        return this.http.post<OwnerEntity>(`owners`, entity);
      })
    )
  };

  editOwner(aOwner: OwnerEntity): Observable<OwnerEntity> {
    return this.http.put(`owners`, aOwner);
  };
  deleteOwner(aOwnerId: number): Observable<OwnerEntity[]> {
    return this.http.delete(`owners/${aOwnerId}`);
  };
}
