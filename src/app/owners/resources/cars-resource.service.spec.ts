import { TestBed } from '@angular/core/testing';

import { CarsResourceService } from './cars-resource.service';

describe('CarsResourceService', () => {
  let service: CarsResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarsResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
