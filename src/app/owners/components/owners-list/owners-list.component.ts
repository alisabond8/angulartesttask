import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {OwnerEntity} from "../../interfaces/OwnerEntity";
import {CarsResourceService} from "../../resources/cars-resource.service";
import {switchMap} from "rxjs/operators";
@Component({
  selector: 'app-owners-list',
  templateUrl: './owners-list.component.html',
  styleUrls: ['./owners-list.component.scss']
})
export class OwnersListComponent implements OnInit {
  owners$: Observable<OwnerEntity[]> | undefined;
  selectedEntityId: number = 0;
  constructor(
    private carsResource: CarsResourceService
  ) { }

  ngOnInit(): void {
    this.owners$ = this.carsResource.getOwners();
  }

  onItemClick(event: MouseEvent | TouchEvent): void {
    let currentNode = event.target as HTMLElement;
    let id;
    while (!id && currentNode != event.currentTarget) {
      id = (currentNode as HTMLElement).getAttribute('data-id');
      currentNode = currentNode.parentNode as HTMLElement;
    }
    if (id) {
      this.selectedEntityId = +id;
    }
  }

  removeCar(): void {
    this.owners$ = this.carsResource.deleteOwner(this.selectedEntityId).pipe(
      switchMap(() => {
        this.selectedEntityId = 0;
        return this.carsResource.getOwners();
      })
    );

  }

}
