import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {CarsResourceService} from "../../resources/cars-resource.service";
import {OwnersValidators} from "../../classes/OwnersValidators";
import {OwnerEntity} from "../../interfaces/OwnerEntity";

@Component({
  selector: 'app-owner-edit',
  templateUrl: './owner-edit.component.html',
  styleUrls: ['./owner-edit.component.scss']
})
export class OwnerEditComponent implements OnInit {
  action: 'add' | 'edit' | 'view' | undefined;
  id: number | undefined;
  form: FormGroup = new FormGroup({});

  get carsFormGroup (): FormArray {
    return this.form.get('cars') as FormArray
  }

  get isView(): boolean {
    return this.action === 'view';
  }

  constructor(
    private route: ActivatedRoute,
    private carsResource: CarsResourceService,
    private router: Router
  ) { }

  ngOnInit(): void {
    const snapshot = this.route.snapshot;
    this.action = snapshot.params.action || 'add';
    this.id = +snapshot.params.id || 0;

    this.form = new FormGroup({
      id: new FormControl(null),
      firstName: new FormControl('', [
        Validators.required,
        OwnersValidators.alpha,
        Validators.minLength(1),
        Validators.maxLength(100)
      ]),
      middleName: new FormControl('', [
        Validators.required,
        OwnersValidators.alpha,
        Validators.minLength(1),
        Validators.maxLength(100)
      ]),
      lastName: new FormControl('', [
        Validators.required,
        OwnersValidators.alpha,
        Validators.minLength(1),
        Validators.maxLength(100)
      ]),
      cars: new FormArray([])
    });
    if (this.action === 'add') {
      this.addNewCar();
    } else {
      const owner: OwnerEntity = snapshot.data.owner;
      owner.cars.forEach(car => {
        this.addNewCar();
      });
      this.form.setValue(owner);
      if (this.action === 'view') {
        this.form.disable();
      }
    }
    if(this.action === 'view') {
      this.form.disable();
    }
  }

  createCarForm(): FormGroup {
    return new FormGroup({
      registrationNumber: new FormControl('', [
        Validators.required,
        OwnersValidators.registrationNumberFormat,
      ]),
      vendor: new FormControl('', [
        Validators.required,
        OwnersValidators.alpha,
        Validators.minLength(1),
        Validators.maxLength(100)
      ]),
      model: new FormControl('', [
        Validators.required,
        OwnersValidators.alphaNumeric,
        Validators.minLength(1),
        Validators.maxLength(100)
      ]),
      manufacturedIn: new FormControl('', [
        Validators.required,
        OwnersValidators.numeric,
        Validators.max(2021),
        Validators.min(1990)
      ]),
    });
  }

  submit(): void {
    const v = this.form.value;
    switch (this.action) {
      case 'add':
        this.carsResource.createOwner(v.lastName, v.firstName, v.middleName, v.cars).subscribe(v => {
          this.router.navigate(['/']);
        })
        break;
      case 'edit':
        this.carsResource.editOwner(v).subscribe(v =>{
          this.router.navigate(['/']);
        });
        break;
    }
  }

  addNewCar(): void {
    (this.form.get('cars') as FormArray).push(this.createCarForm());
  }

  removeCar(index: number): void {
    (this.form.get('cars') as FormArray).removeAt(index);
  }

}
