import {FormControl} from "@angular/forms";

export class OwnersValidators {
  static registrationNumberFormat(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[A-Z]{2}[0-9]{4}[A-Z]{2}$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { registrationNumberFormat: true };
  }

  static alpha(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[\-A-Za-zа-яА-Я]*$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { alpha: true };
  }

  static alphaNumeric(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[\-A-Za-zа-яА-Я0-9]*$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { alphaNumeric: true };
  }

  static numeric(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[0-9]*$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { numeric: true };
  }
}
