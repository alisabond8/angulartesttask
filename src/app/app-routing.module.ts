import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import  {OwnersListComponent } from "./owners/components/owners-list/owners-list.component";
import { OwnerEditComponent } from "./owners/components/owner-edit/owner-edit.component";
import {OwnerResolver} from "./owners/resolvers/owner.resolver";

const routes: Routes = [
  {
    path: '',
    component: OwnersListComponent
  },
  {
    path: 'owners/:action/id/:id',
    component: OwnerEditComponent,
    resolve: {
      owner: OwnerResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
